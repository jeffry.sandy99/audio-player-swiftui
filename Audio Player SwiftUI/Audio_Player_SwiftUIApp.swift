//
//  Audio_Player_SwiftUIApp.swift
//  Audio Player SwiftUI
//
//  Created by Jeffry Sandy Purnomo on 08/11/20.
//

import SwiftUI

@main
struct Audio_Player_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
